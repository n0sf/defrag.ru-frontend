export interface MulticupResultInterface {
    country: string;
    id: string;
    minround: number;
    nick: string;
    rating: string;
    round1: number;
    round2: number;
    round3: number;
    round4: number;
    sum: number;
}
