import { Component } from '@angular/core';

@Component({
    templateUrl: './archive.page.html',
    styleUrls: ['./archive.page.less'],
})
export class ArchivePageComponent {}
