export enum NavigationPages {
    MAIN = '',
    RATING = 'rating',
    TEAMS = 'teams',
    RULES = 'rules',
    ARCHIVE = 'archive',
    MOVIES = 'movies',
}