import { NgModule } from '@angular/core';
import { LanguageService } from '../services/language/language.service';

@NgModule({
    providers: [LanguageService],
})
export class GlobalServicesModule {}
